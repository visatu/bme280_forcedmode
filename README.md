# BME280_forced
Library for forced mode usage of the BME280.

Based on the Seeed Studio [BME280_Grove library](https://github.com/Seeed-Studio/Grove_BME280), and as such uses the MIT license.

The sensor is used in forced mode, meaning a reading must be initiated manually by calling `measure()`. The function forces a single measurement and reads the udpated raw values from the sensor. These raw values can then be calculated ("compensated"), and returned by the `calcTemp`, `calcPres`, and `calcHumi` functions.

- calcTemp returns the temperature in °C
- calcPres returns the pressure in hPa
- calcHumi returns the humidity in relative humidity %

Note that the pressure and humidity compensation formulas depend on a value calculated based on the temperature reading. For this reason the `calcTemp()` function is advisable to be run first whenever calculating the compensated pressure or humidity readings.

For example, to just get the pressure reading:
```cpp
bme.measure(); // update and read raw values from sensor
bme.calcTemp(); // calculate temp to get updated t_fine value used in pressure compensation
pressurehPa = bme.calcPres(); // calculate compensated pressure.
```

## Example usage

```cpp
#include <Arduino.h>
#include <Wire.h>
#include "bme280.hpp"
// CONFIG

#define addr_BME 0x76
#define LOOP_DELAY_MS 2000

BME280 bme;

void setup()
{
	Serial.begin(115200);
	bme.init(addr_BME);
}

void loop()
{
	bme.measure();
	Serial.print("BME: ");
	Serial.print(bme.calcTemp()); Serial.print(" °C, ");
	Serial.print(bme.calcPres()); Serial.print(" hPa, ");
	Serial.print(bme.calcHumi()); Serial.print(" %RH ");
	Serial.println();
	delay(LOOP_DELAY_MS);
}

```
