// BME280 library based on the Seeed Studio Grove_BME280 library:
// https://github.com/Seeed-Studio/Grove_BME280
//
// Modified to use "Forced mode", eg. manual readings from sensor.
// all measurements set to 16x oversampling --> meas time ~100 ms
// use measure() to update raw values,
// calcXXXX to return the compensated reading.
#include "bme280.hpp"

bool BME280::init(int i2c_addr) {
	uint8_t retry = 0;
	uint8_t chip_id = 0;
	devAddr = i2c_addr;
	Wire.begin();
	while ((retry++ < 5) && (chip_id != 0x60)) {
		chip_id = BME280Read8(REG_CHIPID);
		#ifdef BMP280_DEBUG_PRINT
		Serial.print("Read chip ID: ");
		Serial.println(chip_id);
		#endif
		delay(100);
	}
	if (chip_id != 0x60){
		Serial.println("Read Chip ID fail!");
		return false;
	}
	// read calibration data
	dig_T1 = BME280Read16LE(REG_DIG_T1);
	dig_T2 = BME280ReadS16LE(REG_DIG_T2);
	dig_T3 = BME280ReadS16LE(REG_DIG_T3);

	dig_P1 = BME280Read16LE(REG_DIG_P1);
	dig_P2 = BME280ReadS16LE(REG_DIG_P2);
	dig_P3 = BME280ReadS16LE(REG_DIG_P3);
	dig_P4 = BME280ReadS16LE(REG_DIG_P4);
	dig_P5 = BME280ReadS16LE(REG_DIG_P5);
	dig_P6 = BME280ReadS16LE(REG_DIG_P6);
	dig_P7 = BME280ReadS16LE(REG_DIG_P7);
	dig_P8 = BME280ReadS16LE(REG_DIG_P8);
	dig_P9 = BME280ReadS16LE(REG_DIG_P9);

	dig_H1 = BME280Read8(REG_DIG_H1);
	dig_H2 = BME280Read16LE(REG_DIG_H2);
	dig_H3 = BME280Read8(REG_DIG_H3);
	dig_H4 = (BME280Read8(REG_DIG_H4) << 4) | (0x0F & BME280Read8(REG_DIG_H4 + 1));
	dig_H5 = (BME280Read8(REG_DIG_H5 + 1) << 4) | (0x0F & BME280Read8(REG_DIG_H5) >> 4);
	dig_H6 = (int8_t)BME280Read8(REG_DIG_H6);

	// 16x oversampling for humidity
	writeRegister(REG_CTRL_HUM, ctrl_hum);
	// 16x oversampling for temp and pressure, set to forced mode
	writeRegister(REG_CTRL_MEAS, ctrl_meas);

	return true;
}

void BME280::softReset() {
	writeRegister(REG_SOFTRESET, 0xB6);
}
// update measurements. Takes ~116 ms
void BME280::measure() {
	// set to forced mode for a single
	writeRegister(REG_CTRL_MEAS, ( (ctrl_meas & 0b11111100) | 1 ) ) ;
	// wait for worst case meastime at 16x all
	delay(115);
	// burst read measurement data from registers
	Wire.beginTransmission(devAddr);
	Wire.write(0xF7); // Pressure MSB register
	Wire.endTransmission(false);
	Wire.requestFrom(devAddr, 8);				  	// request 8 bytes
	rawValues[0] = Wire.read() << 16 | Wire.read() << 8 | Wire.read(); // 0xF7, 0xF9 - Pressure
	rawValues[1] = Wire.read() << 16 | Wire.read() << 8 | Wire.read(); // 0xFA, 0xFC - Temp
	rawValues[2] = Wire.read() << 8 | Wire.read(); // 0xFD, 0xFE - Humidity
}

// Returns compensated temp readint (°C),
// updates 't_fine' used to calculate pressure and humidity readings
float BME280::calcTemp() {
	int32_t var1, var2;
	int32_t adc_T = rawValues[1];

	// compensation formula from datasheet :D
	adc_T >>= 4;
	var1 = (((adc_T >> 3) - ((int32_t)(dig_T1 << 1))) *
			((int32_t)dig_T2)) >> 11;

	var2 = (((((adc_T >> 4) - ((int32_t)dig_T1)) *
			  ((adc_T >> 4) - ((int32_t)dig_T1))) >> 12) *
			((int32_t)dig_T3)) >> 14;

	t_fine = var1 + var2;
	float T = (t_fine * 5 + 128) >> 8;
	return T / 100;
}

// Returns compensated pressure reading (hPa).
// Uses last calcTemp() results for compensation!
uint32_t BME280::calcPres() {
	int64_t var1, var2, p;
	int32_t adc_P = rawValues[0];

	// compensation formula from datasheet :D
	adc_P >>= 4;
	var1 = ((int64_t)t_fine) - 128000;
	var2 = var1 * var1 * (int64_t)dig_P6;
	var2 = var2 + ((var1 * (int64_t)dig_P5) << 17);
	var2 = var2 + (((int64_t)dig_P4) << 35);
	var1 = ((var1 * var1 * (int64_t)dig_P3) >> 8) + ((var1 * (int64_t)dig_P2) << 12);
	var1 = (((((int64_t)1) << 47) + var1)) * ((int64_t)dig_P1) >> 33;
	if (var1 == 0) {
		return 0; // avoid exception caused by division by zero
	}
	p = 1048576 - adc_P;
	p = (((p << 31) - var2) * 3125) / var1;
	var1 = (((int64_t)dig_P9) * (p >> 13) * (p >> 13)) >> 25;
	var2 = (((int64_t)dig_P8) * p) >> 19;
	p = ((p + var1 + var2) >> 8) + (((int64_t)dig_P7) << 4);
	return (uint32_t)p / 25600;
}

// Returns compensated humidity reading (%RH).
// Uses last calcTemp() results for compensation!
uint32_t BME280::calcHumi() {
	int32_t v_x1_u32r, adc_H;
	adc_H = rawValues[2];

	// compensation formula from datasheet :D
	v_x1_u32r = (t_fine - ((int32_t)76800));
	v_x1_u32r = (((((adc_H << 14) - (((int32_t)dig_H4) << 20) - (((int32_t)dig_H5) * v_x1_u32r)) + ((
					   int32_t)16384)) >> 15) * (((((((v_x1_u32r * ((int32_t)dig_H6)) >> 10) * (((v_x1_u32r * ((int32_t)dig_H3)) >> 11) + ((
								   int32_t)32768))) >> 10) + ((int32_t)2097152)) * ((int32_t)dig_H2) + 8192) >> 14));
	v_x1_u32r = (v_x1_u32r - (((((v_x1_u32r >> 15) * (v_x1_u32r >> 15)) >> 7) * ((int32_t)dig_H1)) >> 4));
	v_x1_u32r = (v_x1_u32r < 0 ? 0 : v_x1_u32r);
	v_x1_u32r = (v_x1_u32r > 419430400 ? 419430400 : v_x1_u32r);
	return (uint32_t)(v_x1_u32r >> 12) / 1024.0;
}

uint8_t BME280::BME280Read8(uint8_t reg) {
	Wire.beginTransmission(devAddr);
	Wire.write(reg);
	Wire.endTransmission();

	Wire.requestFrom(devAddr, 1);
	// return 0 if slave didn't response
	if (Wire.available() < 1) {
		isTransport_OK = false;
		return 0;
	} else {
		isTransport_OK = true;
	}

	return Wire.read();
}

uint16_t BME280::BME280Read16(uint8_t reg) {
	uint8_t msb, lsb;

	Wire.beginTransmission(devAddr);
	Wire.write(reg);
	Wire.endTransmission();

	Wire.requestFrom(devAddr, 2);
	// return 0 if slave didn't response
	if (Wire.available() < 2) {
		isTransport_OK = false;
		return 0;
	} else {
		isTransport_OK = true;
	}
	msb = Wire.read();
	lsb = Wire.read();

	return (uint16_t) msb << 8 | lsb;
}

uint16_t BME280::BME280Read16LE(uint8_t reg) {
	uint16_t data = BME280Read16(reg);
	return (data >> 8) | (data << 8);
}

int16_t BME280::BME280ReadS16(uint8_t reg) {
	return (int16_t)BME280Read16(reg);
}

int16_t BME280::BME280ReadS16LE(uint8_t reg) {
	return (int16_t)BME280Read16LE(reg);
}

uint32_t BME280::BME280Read24(uint8_t reg) {
	uint32_t data;

	Wire.beginTransmission(devAddr);
	Wire.write(reg);
	Wire.endTransmission();

	Wire.requestFrom(devAddr, 3);
	// return 0 if slave didn't response
	if (Wire.available() < 3) {
		isTransport_OK = false;
		return 0;
	} else if (isTransport_OK == false) {
		isTransport_OK = true;
		if (!init(devAddr)) {
			#ifdef BMP280_DEBUG_PRINT
			Serial.println("Device not connected or broken!");
			#endif
		}
	}
	data = Wire.read();
	data <<= 8;
	data |= Wire.read();
	data <<= 8;
	data |= Wire.read();

	return data;
}

void BME280::writeRegister(uint8_t reg, uint8_t val) {
	Wire.beginTransmission(devAddr); // start transmission to device
	Wire.write(reg);       // send register address
	Wire.write(val);         // send value to write
	Wire.endTransmission();     // end transmission
}