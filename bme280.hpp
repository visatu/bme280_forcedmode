#pragma once

#ifndef _BME280_H_
#define _BME280_H_

#include <Arduino.h>
#include <Wire.h>

// device i2c address
#define BME280_ADDRESS   0x76


// calibration data registers
#define REG_DIG_T1    0x88
#define REG_DIG_T2    0x8A
#define REG_DIG_T3    0x8C

#define REG_DIG_P1    0x8E
#define REG_DIG_P2    0x90
#define REG_DIG_P3    0x92
#define REG_DIG_P4    0x94
#define REG_DIG_P5    0x96
#define REG_DIG_P6    0x98
#define REG_DIG_P7    0x9A
#define REG_DIG_P8    0x9C
#define REG_DIG_P9    0x9E

#define REG_DIG_H1    0xA1
#define REG_DIG_H2    0xE1
#define REG_DIG_H3    0xE3
#define REG_DIG_H4    0xE4
#define REG_DIG_H5    0xE5
#define REG_DIG_H6    0xE7

// device info registers
#define REG_CHIPID          0xD0
#define REG_VERSION         0xD1


#define REG_SOFTRESET       0xE0	// soft reset register, write 0xF6 to reset device

#define REG_CAL26           0xE1 	// calibration data starts here


#define REG_CTRL_HUM        0xF2	// humidity control register
#define REG_CTRL_MEAS		0xF4	// temp, pressure control, device mode control
#define REG_CONFIG          0xF5	// t_sb, filter, spi3w_en

#define REG_PRESSUREDATA    0xF7
#define REG_TEMPDATA        0xFA
#define REG_HUMIDITYDATA    0xFD

class BME280 {
	public:
		bool init(int i2c_addr = BME280_ADDRESS);
		void softReset();
		void measure();
		float 	calcTemp();
		uint32_t calcPres();
		uint32_t calcHumi();
	private:
		int32_t rawValues [3]; 			// raw 20bit values [ P, T, H ]
		uint8_t ctrl_hum = 0b00000101; 	// 16x oversampling for humidity
		uint8_t ctrl_meas = 0b10110101; 	// 16x oversampling for temp and pressure, forced mode
		int devAddr;
		bool isTransport_OK;


		// Calibration data
		uint16_t dig_T1;
		int16_t dig_T2;
		int16_t dig_T3;
		uint16_t dig_P1;
		int16_t dig_P2;
		int16_t dig_P3;
		int16_t dig_P4;
		int16_t dig_P5;
		int16_t dig_P6;
		int16_t dig_P7;
		int16_t dig_P8;
		int16_t dig_P9;
		uint8_t dig_H1;
		int16_t dig_H2;
		uint8_t dig_H3;
		int16_t dig_H4;
		int16_t dig_H5;
		int8_t  dig_H6;
		int32_t t_fine;

		// private functions
		uint8_t BME280Read8(uint8_t reg);
		uint16_t BME280Read16(uint8_t reg);
		uint16_t BME280Read16LE(uint8_t reg);
		int16_t BME280ReadS16(uint8_t reg);
		int16_t BME280ReadS16LE(uint8_t reg);
		uint32_t BME280Read24(uint8_t reg);
		void writeRegister(uint8_t reg, uint8_t val);
};

#endif